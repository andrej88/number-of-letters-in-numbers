import java.math.*

val namesEnglish: List<Pair<BigInteger, String>> = listOf(Pair(BigInteger.valueOf(1L), "One"),
                                                          Pair(BigInteger.valueOf(2L), "Two"),
                                                          Pair(BigInteger.valueOf(3L), "Three"),
                                                          Pair(BigInteger.valueOf(4L), "Four"),
                                                          Pair(BigInteger.valueOf(5L), "Five"),
                                                          Pair(BigInteger.valueOf(6L), "Six"),
                                                          Pair(BigInteger.valueOf(7L), "Seven"),
                                                          Pair(BigInteger.valueOf(8L), "Eight"),
                                                          Pair(BigInteger.valueOf(9L), "Nine"),
                                                          Pair(BigInteger.valueOf(10L), "Ten"),
                                                          Pair(BigInteger.valueOf(11L), "Eleven"),
                                                          Pair(BigInteger.valueOf(12L), "Twelve"),
                                                          Pair(BigInteger.valueOf(13L), "Thirteen"),
                                                          Pair(BigInteger.valueOf(14L), "Fourteen"),
                                                          Pair(BigInteger.valueOf(15L), "Fifteen"),
                                                          Pair(BigInteger.valueOf(16L), "Sixteen"),
                                                          Pair(BigInteger.valueOf(17L), "Seventeen"),
                                                          Pair(BigInteger.valueOf(18L), "Eighteen"),
                                                          Pair(BigInteger.valueOf(19L), "Nineteen"),
                                                          Pair(BigInteger.valueOf(20L), "Twenty"),
                                                          Pair(BigInteger.valueOf(30L), "Thirty"),
                                                          Pair(BigInteger.valueOf(40L), "Forty"),
                                                          Pair(BigInteger.valueOf(50L), "Fifty"),
                                                          Pair(BigInteger.valueOf(60L), "Sixty"),
                                                          Pair(BigInteger.valueOf(70L), "Seventy"),
                                                          Pair(BigInteger.valueOf(80L), "Eighty"),
                                                          Pair(BigInteger.valueOf(90L), "Ninety"),
                                                          Pair(BigInteger.valueOf(100L), "One Hundred"),
                                                          Pair(BigInteger.valueOf(1000L), "One Thousand"),
                                                          Pair(BigInteger.valueOf(1000000L), "One Million"),
                                                          Pair(BigInteger.valueOf(1000000000L), "One Billion"),
                                                          Pair(BigInteger.valueOf(1000000000000L), "One Trillion"),
                                                          Pair(BigInteger.valueOf(1000000000000000L), "One Quadrillion"),
                                                          Pair(BigInteger.valueOf(1000000000000000000L), "One Quintillion"))

val names = namesEnglish

fun main(args: Array<String>) {
    showLongestChains(BigInteger("1" + "000" + "000" + "000"))
}

/**
 * Starting at 1 and going up by [step] each time, prints the word forms of
 * numbers less than [numTrials]
 */
fun printFirstN(numTrials: BigInteger, step: BigInteger = BigInteger.ONE) {
    var i = BigInteger.valueOf(1)
    while (i <= numTrials && i > BigInteger.ZERO) {
        println("${"%,20d".format(i)}   ${nameOfNumber(i)}")
        i += step
    }
}

/**
 * Count the chain lengths of every number between 1 and [numTrials] (inclusive),
 * and prints the 20 smallest numbers with a chain length equal to the max.
 */
fun showLongestChains(numTrials: BigInteger, startingValue: BigInteger = BigInteger.ONE, step: BigInteger = BigInteger.ONE) {
    var i = startingValue
    val pairs = mutableListOf<Pair<BigInteger, BigInteger>>()
    var progressUnits = BigInteger.valueOf(1)
    var progressSmall = 1
    var progressBig = 1
    val oneProgressUnit = BigInteger.valueOf(1000L)
    while (i >= BigInteger.ONE && i < numTrials) {
        pairs.add(Pair(i, stepsUntilThreshold(i)))
        i += step
        // Print a "progress bar" (total 1000 periods)
        if (numTrials > oneProgressUnit) {
            if (i >= (progressUnits * (numTrials / oneProgressUnit))) {
                print(".")
                progressUnits += BigInteger.ONE
                progressSmall++
            }
            if (progressSmall >= 100) {
                progressSmall = 1
                progressBig++
                println()
            }
        }
    }
    println()
    val max = pairs.maxBy { it.second }!!.second
    println("The greatest number of steps is $max, accomplished by the following numbers:")
    var numMax = 1
    val numMaxToDisplay = 20
    for (j in pairs) {
        if (j.second == max) {
            stepsUntilThreshold(j.first, print = true)
            numMax++
        }
        if (numMax > numMaxToDisplay) {
            val count = pairs.count { it.second == max }
            println("and ${count - numMaxToDisplay} more ($count total)")
            break
        }
    }
}

/**
 * Counts the chain length of [number]. In English, 4 returns 1 (and is the only number to do so).
 */
fun stepsUntilThreshold(number: BigInteger, print: Boolean = false): BigInteger {
    fun _stepsUntilThreshold(number: BigInteger, visited: List<BigInteger>): BigInteger =
            if (number in visited) {
                if (print) println()
                BigInteger.valueOf(visited.size.toLong())
            }
            else {
                if (print) {
                    if (visited.isNotEmpty()) {
                        print(" → ")
                    }
                    print("$number")
                }
                _stepsUntilThreshold(lengthOfNameOfNumber(number), visited + number)
            }

    return _stepsUntilThreshold(number, emptyList())
}

/**
 * Counts the number of non-space characters in the word form of [number]
 */
fun lengthOfNameOfNumber(number: BigInteger) = BigInteger.valueOf(nameOfNumber(number).count { it != ' ' }.toLong())!!

val _100 = BigInteger.valueOf(100L)!!
val _1000 = BigInteger.valueOf(1000L)!!

/**
 * Returns the word form of [number]
 */
fun nameOfNumber(number: BigInteger): String {
    val numberFound: Pair<BigInteger, String>? = names.find { it.first == number }
    if (numberFound != null) {
        return numberFound.second
    }
    else {
        var returnVal = ""

        val numberLength = BigInteger.valueOf(number.toString().length.toLong())
        val _3 = BigInteger.valueOf(3)
        var numGroups = numberLength / _3
        if (numberLength % _3 != BigInteger.ZERO) numGroups += BigInteger.ONE

        var i = if (numGroups == BigInteger.ZERO) BigInteger.ONE else numGroups
        while (i >= BigInteger.ONE) {

            val order = BigInteger.TEN.pow(3 * (i.toInt() - 1))
            returnVal += "${nameOfLessThan1000(number / order)}"
            if (i > BigInteger.ONE) {
                var name = names.find { it.first == order }!!.second
                if (name.substring(0, 3) == "One") name = name.substring(4)
                returnVal += " $name "
            }

            i -= BigInteger.ONE
        }
        return returnVal.trim()
    }
}

/**
 * Gets the name of a group of 3 numbers (e.g, "512" becomes "Five Hundred Twelve", "11" becomes 11)
 */
fun nameOfLessThan1000(number: BigInteger): String {

    val workingNumber = number % _1000

    val justHundreds = workingNumber.divide(_100) * _100
    val justTensAndOnes = workingNumber % _100
    val justTens = justTensAndOnes.divide(BigInteger.TEN) * BigInteger.TEN
    val justOnes = workingNumber % BigInteger.TEN

    val nameOfWholeNumberPair = names.find { it.first == workingNumber }
    if (nameOfWholeNumberPair != null) {
        return nameOfWholeNumberPair.second
    }

    val nameOfHundredsPair = names.find { it.first == justHundreds }
    val nameOfTensAndOnesPair = names.find { it.first == justTensAndOnes }

    val nameOfHundredsDigit = names.find { it.first == justHundreds / _100 }
    val nameOfHundreds = nameOfHundredsPair?.second ?: if (nameOfHundredsDigit != null) "${nameOfHundredsDigit.second} Hundred" else ""
    val nameOfTensPair = names.find { it.first == justTens }
    val nameOfOnesPair = names.find { it.first == justOnes }
    val nameOfTensAndOnes = nameOfTensAndOnesPair?.second ?:
                            "${nameOfTensPair?.second ?: ""} ${nameOfOnesPair?.second ?: ""}"

    return if (nameOfHundreds != "") (nameOfHundreds + " " + nameOfTensAndOnes).trim() else nameOfTensAndOnes.trim()
}